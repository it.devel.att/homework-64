class FollowersUsers < ActiveRecord::Migration[5.2]
    def change
      create_table :followers_users, id: false, :force => true do |t|
        t.integer :follower_id
        t.integer :user_id

        t.timestamps
    end
  end
end
