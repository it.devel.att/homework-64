class ChangeColumnInFollowersUsers < ActiveRecord::Migration[5.2]
  def change
    rename_column :followers_users, :user_id, :following_id
  end
end
