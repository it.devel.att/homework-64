def single_image_post(obj, login)
  for i in 1..2
    post = obj.posts.build(desc: Faker::Lorem.paragraph_by_chars(rand(10..100), false))
    path = Rails.root.join('app', 'assets', 'images', 'fixtures', login, "#{i}.jpg")
    post.images.attach(io: File.open(path), filename: "#{i}.jpg")
    post.save
  end
end

def multi_image_post(obj, login)
  post = obj.posts.build(desc: Faker::Lorem.paragraph_by_chars(rand(10..100), false))
  for i in 3..5
    path = Rails.root.join('app', 'assets', 'images', 'fixtures', login, "#{i}.jpg")
    post.images.attach(io: File.open(path), filename: "#{i}.jpg")
    post.save
  end
end
logins = ['haisenberg', 'jane_loves_cat', 'nature_man']

User.create(name: 'Sample User', email: 'sample@user.ru', login: 'sample_user', password: 1234567, password_confirmation: 1234567)

for login in logins
  user = User.create(name: Faker::Name.name, email: Faker::Internet.unique.email, login: login, password: 1234567, password_confirmation: 1234567)
  multi_image_post(user, user.login)
  single_image_post(user, user.login)
end
