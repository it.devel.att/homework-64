Rails.application.routes.draw do
  devise_for :users
  root 'posts#index'

  resources :users, only: [:show] do
    member do
      post :follow
      post :unfollow
    end

    resources :posts, except: [:index] do
      resources :likes do
        member do
          post :up
          post :down
        end
      end

      collection do
        get :subscriptions
      end

      resources :comments, only: [:create,:destroy]
    end

  end

  resources :posts, only: [] do
    resources :likes, only: [] do
      collection do
        post :up
        delete :down
      end
    end
  end

end
