class UsersController < ApplicationController
  before_action :authenticate_user!, only:[:follow, :unfollow]
  before_action :check_current_user_not_post_user_owner, only:[:follow]

  def index
    @users = User.all
  end

  def show
    @post = Post.new
    @profile_owner = User.find(params[:id])
    @posts = @profile_owner.posts.reverse_order
  end

  def follow
    follows_user = User.find(params[:id])
    current_user.followings << follows_user

    redirect_to root_path
  end

  def unfollow
    follows_user = User.find(params[:id])
    current_user.followings.destroy(follows_user)

    redirect_to root_path
  end

  def check_current_user_not_post_user_owner
    if current_user == User.find(params[:id])
      flash[:danger] = "You can't follow yourself!"
      redirect_back(fallback_location: root_path)
    end
  end

end
