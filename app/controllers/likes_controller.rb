class LikesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_post, only: [:up, :down]
  before_action :check_likes_up, only: [:up]
  before_action :check_likes_down, only: [:down]

  include LikesHelper

  def up
    @like = @post.likes.build
    @like.user = current_user
    @like.save

    flash[:success] = "Success like!"
    redirect_back(fallback_location: root_path)
  end

  def down
    @like = Like.find_by(post_id: @post.id, user_id: current_user.id)
    @like.destroy

    flash[:success] = "Success dislike!"
    redirect_back(fallback_location: root_path)
  end

  private
  def check_likes_up
    if user_leave_like_to?(@post)
      flash[:danger] = "You already leave like!"
      redirect_back(fallback_location: root_path)
    end
  end

  def check_likes_down
    unless user_leave_like_to?(@post)
      flash[:danger] = "You dont'l leave like to this post!"
      redirect_back(fallback_location: root_path)
    end
  end

  def set_post
    @post = Post.find(params[:post_id])
  end
end
