class PostsController < ApplicationController
  before_action :authenticate_user!, only: [:edit, :new, :update, :delete]
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_action :set_user, only: [:create, :show, :update, :destroy]
  before_action :check_current_user_with_post_user, only: [:update, :destroy]

  def index
    @posts = Post.all.reverse_order
  end

  def show
    @comment = Comment.new
    @comments = @post.comments
  end

  def new
    @post = Post.new
  end

  def edit
    check_limit_time_for_edit_post
  end

  def create
    @post = @user.posts.build(post_params)

    @post.images.attach(params[:post][:images])
    if @post.save
      flash[:success] = 'Post was successfully created.'
      redirect_to user_path(@user)
    else
      render :new
    end
  end

  def update
    if @post.update(post_params)
      upload_images
      flash[:success] = 'Post was successfully update.'
      redirect_to user_post_path(@user.id, @post)
    else
      render :edit
    end
  end

  def destroy
    @post.destroy
    flash[:success] = 'Post was successfully destroy.'
    redirect_to @user
  end

  def subscriptions
    @subscriptions = current_user.followings
    @posts = Post.all.where(user_id: @subscriptions.ids).order(created_at: 'desc')
  end

  def upload_images
    if params[:post][:change_all] && params[:post][:change_all] == "1"
      @post.images.purge
    end
    @post.images.attach(params[:post][:images]) if params[:post][:images].present?
  end

  private
  def check_current_user_with_post_user
    unless current_user == @user && @post.user == @user
      flash[:danger] = 'Incorect user!'
      redirect_back(fallback_location: root_path )
    end
  end

  def check_limit_time_for_edit_post
    if Time.new > @post.created_at + 15.minutes
      flash[:danger] = "You can't edit post after 15 minutes from creating"
      redirect_back(fallback_location: root_path)
    end
  end

  def set_post
    @post = Post.find(params[:id])
  end

  def set_user
    @user =  User.find(params[:user_id])
  end

  def post_params
    params.require(:post).permit(:desc)
  end
end
