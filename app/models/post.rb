class Post < ApplicationRecord
  belongs_to :user
  has_many_attached :images
  has_many :comments, :dependent => :destroy
  has_many :likes, :dependent => :destroy
  has_many :users, through: :likes

  validates :images, attached: true, content_type: ['image/png', 'image/jpg', 'image/jpeg']
  validate :check_limit_for_edit_post, :on => :update

  def check_limit_for_edit_post
    if Time.new > self.created_at + 15.minutes
      errors.add(:post,"Can't edit after 15 minutes of creating post!")
    end
  end
end
