class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :validatable

  has_many :posts, :dependent => :destroy
  has_many :comments
  has_many :likes


  has_and_belongs_to_many :followers, :class_name => "User", :join_table => 'followers_following', foreign_key: :follower_id, :association_foreign_key => :following_id
  has_and_belongs_to_many :followings, :class_name => "User", :join_table => 'followers_following', foreign_key: :following_id, :association_foreign_key => :follower_id


  validates :login, presence: true, uniqueness: true, length: { in: 5..20 }
  validates :name, presence: true, length: { in: 2..20 }
end
