module PostsHelper

  def check_for_subscribe(post)
    true if user_signed_in? && !current_user.followings.include?(post.user) && current_user != post.user
  end

  def check_for_unsubscribe(post)
    true if user_signed_in? && current_user.followings.include?(post.user)
  end

  def check_for_edit(post)
    true if user_signed_in? && current_user == post.user && Time.new < post.created_at + 15.minutes && !current_page?(edit_user_post_path(current_user, post))
  end

  def check_for_delete(post)
    true if user_signed_in? && current_user == post.user && !current_page?(edit_user_post_path(current_user, post))
  end



end
